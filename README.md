# vscode-standardml README

## Features

+ (Limited) syntax highlighting.
+ (Some) snippets.

## Known Issues

+ Syntax highlighting:
  - Multiline string escape used to support multiline string literal in SML97 will not work properly, e.g. this:

    ```
    val my_val = "blah\
    dsfsf
    \blah"
    ```

    really shouldn't be a valid string literal but is rendered so anyway.

## Release Notes

## License

MIT
