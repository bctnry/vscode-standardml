# Roadmap

+ At-least-more-correct syntax highlighting that respects SML97 grammar.
+ Language server support for major implementations:
  - SML/NJ
  - MLton
  - Moscow ML
  - Poly/ML
  - (and more if it's relevant enough & has a language server)
